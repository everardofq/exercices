package fourServ.assigment1;

import java.util.Scanner;

public class EndingTime
{
   public static void main(String [] args)
   {      
	   // Variable declarations
	   // Hint: All variables need to be declared as integers
	   int startingTime = 0;
	   int duration = 0;
	   int totalDuration = 0;
	   int endingHours = 0;
	   int endingMints = 0;
	   String stime="";
	   
	   
	   try (// Create a Scanner object to read from the keyboard
			   Scanner keyboard = new Scanner(System.in)) {
			// Get the starting time in hours and minutes
			   System.out.print("Enter the starting time (in hours and minutes): ");
			   stime=keyboard.nextLine();
			// Get the duration time in minutes
			   System.out.print("Enter the duration in minutes: ");
			   duration=keyboard.nextInt();
			   if(stime.indexOf(" ") < 0)
			   {
				   throw new Exception("Error: There are no space separating hours and minutes in starting time");
			   }
		   
	}catch (Exception e) {
		System.out.println(""+e+"\nPlease use an space between hours and minutes for starting time, i.e. 2 30");
		System.out.println("Olny type numbers..");
	}
	   String numbers[]=stime.split(" ");
	   // Calculate the ending time
	   startingTime=(60*(Integer.parseInt(numbers[0]))+(Integer.parseInt(numbers[1])));
	   totalDuration=duration+startingTime;
	   endingHours=(totalDuration/60);
	   endingMints=(totalDuration%60);
	   if(endingHours >23) {
		   endingHours-=24;
	   }
	   // Display the output
	   System.out.println("Endingtime = "+endingHours+" "+endingMints);	   
   }
}