package fourServ.assigment1;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Pay {
	public static void main(String[] args) {
		// Declare Variables
		double hoursWorked = 0;
		double hourlyPayRate = 0;
		double grossPay = 0;
		double netPay = 0;
		double taxes=0;
		// take the following variables from the user from command promt.
		BufferedReader dataIn= new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.print("How many hours did you work? ");
			hoursWorked = Double.parseDouble(dataIn.readLine());
			System.out.print("How much do you get paid per hour? ");
			hourlyPayRate = Double.parseDouble(dataIn.readLine());
		} catch (Exception e) {
			System.out.println("An error occurred when trying to read the input from keyword...");
		}

		if (hoursWorked <= 40) {
			hoursWorked*=1;
		}else if(hoursWorked >40) {
			hoursWorked*=1.5;
		}
		taxes=(hoursWorked*hourlyPayRate)*0.33;
		grossPay= (hoursWorked*hourlyPayRate);
		netPay= (hoursWorked*hourlyPayRate)-taxes;
		
		// Display the results
		System.out.println("You earned $" + grossPay);
		System.out.println("Taxes are $"+ taxes);
		System.out.println("your net pay is $"+ netPay);
	}
}
