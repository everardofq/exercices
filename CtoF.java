package fourServ.assigment1;

public class CtoF
{
 public static void main (String[] args)
 {
   // Celsius temp is a constant
   // Fahrenheit temp

   final double CELS = 37;
   double FAHR=0;
   //(0 °C × 9/5) + 32 = 32 °F
   FAHR=((CELS*9)/5)+32;
   // calculate Fahrenheit temp using formula
   // display intro message

   System.out.println("This program converts Celsius to Fahrenheit");
   // display the result

   System.out.println("Celsius Temp = " + CELS);
   System.out.println("Fahrenheit Temp = " + FAHR);

   // display end message
   System.out.println("End of program");
 }
}
